package ex2016.a01.t4.sol1;

import java.util.*;

public class BuilderWithoutElements<X> implements ListBuilder<X> {
    
    private final ListBuilder<X> builder;
    private final Collection<X> out;
    
    BuilderWithoutElements(ListBuilder<X> builder, Collection<X> out) {
        super();
        this.builder = builder;
        this.out = out;
    }

    public void add(X x) {
        if (out.contains(x)){
            throw new IllegalArgumentException();
        }
        builder.add(x);
    }

    public List<X> build() {
        return builder.build();
    }
}
